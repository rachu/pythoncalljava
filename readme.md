# Test project to call java from python

The focus of this project is to call java from python within a try-except.
Want to verify that when java returns a non-zero exit code, that python will enter the except.

### Build java

mvn clean package


### Run python

* On mac cmd line: python3 tempchu.py 
* pycharm