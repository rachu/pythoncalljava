import subprocess

print("Hello Python World")

try:
    print("Entered the try")

    subprocess.check_output(['java', '-jar', '//Users/ricardochu/mygitsrc/pythoncalljava/HelloWorld/target/HelloWorld.jar'])


except:
    print("Entered the except")

# References:
#  http://sharats.me/the-ever-useful-and-neat-subprocess-module.html
#  https://docs.python.org/3/library/subprocess.html?highlight=subprocess%20check_output#subprocess.check_output
